const http = require('http')

function printError(e) { console.error(e.message); }
module.exports = function get(city) {
  const req = http.get('http://api.openweathermap.org/data/2.5/weather?q=' +
    city + '&units=metric&apikey=c184205bc1fcbcdc42c4b37ccf710de3', responseFunction);

  function responseFunction(res) {
    let body = '';
    res.on('data', function (chunk) { body += chunk; }); // On getting data, do this
    res.on('end', function () {         // when done with data, do this
      if (res.statusCode === 200) {
        try { let o = JSON.parse(body)
          s = 'In ' + o.name + ', temp is ' + o.main.temp + ' degrees C.'
          console.log(s)
        } catch (error) { printError(error); }
      } else {
        printError({
          message: 'Error getting weather from ' + city + '. (' +
            http.STATUS_CODES[res.statusCode] + ')'
        })
      }
    })
  }
  req.on('error', printError)  // on error, do this
}



